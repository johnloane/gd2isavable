/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2isavableclass;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author administrator
 */
public class main {
    /* Create a simple interface that allows an object to be loaded
    and saved to some sort of storage medium.
    The exact type of medium is not known to the interface.
    The interface will specify two methods, one to return an 
    ArrayList of values values to be saved, the other method
    will take an ArrayList and populate the object from the
    ArrayList.
    Create some sample classes to implement the interface - 
    Players and Monsters. 
    Override the toString() method for each of your classes
    In main write method that takes an object that implements
    the interface as a parameter and "saves" the values.
    */
    public static void main(String args[]){
        Player james = new Player("James", 5, 2, "JarOfFridge");
        System.out.println(james.toString());
        saveObject(james);
        
        james.setHitPoints(8);
        System.out.println(james);
        james.setWeapon("AAAA_AAAAA_AAaa");
        saveObject(james);
        loadObject(james);
        System.out.println(james);
        
        ISavable werewolf = new Monster("Werwolf", 20, 40);
        System.out.println("Strength = " + ((Monster)werewolf).getStrength());
    }
    
    public static void saveObject(ISavable objectToSave){
        for(int i=0; i < objectToSave.write().size(); ++i){
            System.out.println("Saving " + objectToSave.write().get(i) + " to the storage device");
        }
    }
    
    public static void loadObject(ISavable objectToLoad){
        List<String> values = readValues();
        objectToLoad.read(values);
    }
    
    public static List<String> readValues(){
        List<String> values = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        boolean quit = false;
        int index = 0;
        System.out.println("Choose \n" + "1 to enter a string\n" + "0 to quit");
        
        while(!quit){
            System.out.println("Choose an option: ");
            int choice = sc.nextInt();
            sc.nextLine();
            switch(choice){
                case 0:
                    quit = true;
                    break;
                case 1:
                    System.out.println("Enter a string: ");
                    String stringInput = sc.nextLine();
                    values.add(index, stringInput);
                    index++;
                    break;
            }
        }
        return values;
    }
            
    
}
